<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadProgression extends Model
{
    protected $table = 'leadprogression';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
