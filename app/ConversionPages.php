<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConversionPages extends Model
{
    protected $table = 'conversionpage_u';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
