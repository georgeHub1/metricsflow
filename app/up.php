<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class up extends Model
{
    protected $table = 'up';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
