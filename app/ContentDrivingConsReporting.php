<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentDrivingConsReporting extends Model
{
    protected $table = 'contentdrivingconsreporting_u';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
