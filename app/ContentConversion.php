<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentConversion extends Model
{
    protected $table = 'conversionpage';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
