<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TotalConvertReporting extends Model
{
    protected $table = 'totalconvertreporting_u';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
