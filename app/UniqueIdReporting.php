<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UniqueIdReporting extends Model
{
    protected $table = 'uniqueidreporting_u';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
