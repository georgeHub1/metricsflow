<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConversionBySite extends Model
{
    protected $table = 'conversionbysite_u';
    public $timestamps = false;
    protected $connection = 'raw-data';
}
