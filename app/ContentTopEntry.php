<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentTopEntry extends Model
{
    protected $table = 'topentry';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
