<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConversionReporting extends Model
{
    protected $table = 'conversionreporting_u';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
