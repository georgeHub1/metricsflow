<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ABMaggregate extends Model
{
    protected $table = 'ABMaggregate_u';

    public $timestamps = false;

    protected $connection = 'raw-data';
}
