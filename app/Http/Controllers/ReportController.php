<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Input;
use DB;
use App\Leads as Leads;
use App\TotalConvertReporting as TotalConvertReporting;

use App\ConversionBySite as ConversionBySite;
use App\ABMaggregate as AccountBasedMarketing;
use App\ABMCVDetails as ABMCVDetails;
use App\VisitorBreakdown as VisitorBreakdown;

use App\UniqueIdReporting as UniqueIdReporting;
use App\ContentActivitiesReporting as ContentActivitiesReporting;
use App\ConversionReporting as ConversionReporting;

use App\ConversionForms as ConversionForms;
use App\ConversionPages as ConversionPages;

use App\up as up;




class ReportController extends Controller
{
    public function index()
    {
        $client_id=Session('client_id');
        return view('reports', [
            'client_id'=>$client_id
        ]);
    }

    public function detail()
    {
        $client_id=Session('client_id');
        return view('reports_detail', [
            'client_id'=>$client_id,
        ]);
    }
    public function warranty() //Might wanna delete this too
    {
        $client_id=Session('client_id');

        return view('reports_warranty', [
            'client_id'=>$client_id,

        ]);
    }

    public function conversiondetails(Request $request) // Might Delete this
    {
        $PageName=$request->PageName;
        $client_id=Session('client_id');
        return view('ConversionDetails', [
            'client_id'=>$client_id,
        ]);
    }

    public function WarrantyDetails(Request $request)
    {
        $input = Input::only('PageURL','PageName');
        $PageURL = $input['PageURL'];
        $PageName = $input['PageName'];

        $URL1 = parse_url($PageURL, PHP_URL_HOST);
        $URL2 = parse_url($PageURL, PHP_URL_PATH);

        $PageURL = $URL1 . '' . $URL2;

        $client_id=Session('client_id');
        $data = up::where('client_id',$client_id) -> where('conversion_page_e_id_SESSION_WISE', $PageURL ) -> orderBy('Total_Pages_till_conversion_page_e_id_SESSION_WISE', 'DESC')-> where('Channel', '!=' , 'NULL')-> take(5)-> get(['Channel','conversion_page_e_id_SESSION_WISE','1','2x','3x','4x','5x','6x','7x','8x','9x','10x']);
        return view('reports_warrantydetail', [
            'client_id'=>$client_id, 'PageName' => $PageName, 'data' => $data
        ]);
    }

    public function getTotalConvertReporting()
    {
        $client_id = Input::get('clientID');
        $days = Input::get('days');
        $fromDate = date("Y-m-d");
        $toDate = date('Y-m-d', strtotime($fromDate. ' - '.$days.' days'));

        $conversionOverview=TotalConvertReporting::where([['client_id',$client_id],['SystemTime','>',$toDate]])->orderBy('SystemTime')->get();
        return response()->json($conversionOverview);

    }

    public function getConversionBySite()
    {
        $client_id = Input::get('clientID');
        $Conversion_Site=ConversionBySite::where('client_id',$client_id)->where('Site','!=','N/A')->orderBy('freq','desc')->take(4)->get();

        return response()->json($Conversion_Site);
    }

    public function getVisitorsBreakdown()
    {
        $client_id = Input::get('clientID');
        $visitors=VisitorBreakdown::where('client_id',$client_id)->first();
        $data['TotalUniqueIDs'] = $visitors->TotalUniqueusers;
        $data['UniqueIDsBlockingCookies'] = $visitors->UniqueIDBlockCookies;
        $data['AveragePagesConsumed'] = $visitors->avgses;
        $data['TotalContentActivities'] = $visitors->TotalContentActivities;
        $data['TotalUniqueConversions'] = $visitors->UniqueConverions;

        return response()->json($data);
    }

    public function getChannelOverview()
    {
        $client_id = Input::get('clientID');

        $UniqueIds=UniqueIdReporting::where('client_id',$client_id)-> where('Days', 0) ->get();
        $ContentActivitie=ContentActivitiesReporting::where('client_id',$client_id)-> where('Days', 0)->get();
        $Conversion=ConversionReporting::where('client_id',$client_id)-> where('Days', 0)->get();

        $data['UniqueIds'] = $UniqueIds;
        $data['ContentActivitie'] = $ContentActivitie;
        $data['Conversion'] = $Conversion;

        return response()->json($data);
    }

    public function getConversionDetails(){
        $client_id = Input::get('clientID');

        // $days = Input::get('days');
        // $fromDate = date("Y-m-d");
        // $toDate = date('Y-m-d', strtotime($fromDate. ' - '.$days.' days'));

        $conversion_forms=ConversionForms::where('client_id',$client_id)->orderBy('Conversions','desc')->get();
        $conversion_pages=ConversionPages::where('client_id',$client_id)->orderBy('Value','desc')->get();

        $data['ConversionForms'] = $conversion_forms;
        $data['ConversionPages'] = $conversion_pages;

        return response()->json($data);
    }

    public function getAccountBasedMarketing(){
        $client_id = Input::get('clientID');
        $abm_details = AccountBasedMarketing::where('client_id',$client_id)->get();

        return response()->json($abm_details);

    }

    public function getABMCVDetails(){
        $client_id = Input::get('clientID');
        $abm_cvdetails = ABMCVDetails::where('client_id',$client_id)->where('domain','!=','')->get();

        return response()->json($abm_cvdetails);

    }

}
