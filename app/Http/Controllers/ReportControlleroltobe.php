<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Input;
use DB;
use App\Leads as Leads;
use App\TotalConvertReporting as TotalConvertReporting;

use App\ConversionBySite as ConversionBySite;
use App\VisitorBreakdown as VisitorBreakdown;

use App\UniqueIdReporting as UniqueIdReporting;
use App\ContentActivitiesReporting as ContentActivitiesReporting;
use App\ConversionReporting as ConversionReporting;

use App\ConversionForms as ConversionForms;
use App\ABMaggregate as ABMaggregate;
use App\ContentDrivingConsReporting as  ContentDrivingConsReporting;




class ReportController extends Controller
{
    public function index()
    {
        $client_id=Session('client_id');
        return view('reports', [
            'client_id'=>$client_id
        ]);
    }

    public function detail()
    {
        $client_id=Session('client_id');
        $contentDrivingReporting=ContentDrivingConsReporting::where('client_id',$client_id)->orderBy('freq','desc')->get();
        $conversion_forms=ConversionForms::where('client',$client_id)->orderBy('conversions','desc')->get();
        return view('reports_detail', [
            'client_id'=>$client_id,
            'contentDrivingReporting'=>$contentDrivingReporting,
            'conversion_forms'=>$conversion_forms,
        ]);
    }

    public function getTotalConvertReporting()
    {
        $client_id = Input::get('clientID');
        $days = Input::get('days');
        $fromDate = date("Y-m-d");
        $toDate = date('Y-m-d', strtotime($fromDate. ' - '.$days.' days'));

        $conversionOverview=TotalConvertReporting::where([['client_id',$client_id],['SystemTime','>',$toDate]])->orderBy('SystemTime')->get();
        return response()->json($conversionOverview);

    }

    public function getConversionBySite()
    {
        $client_id = Input::get('clientID');
        $Conversion_Site=ConversionBySite::where('client_id',$client_id)->orderBy('freq','desc')->take(6)->get();


        return response()->json($Conversion_Site);
    }

    public function getVisitorsBreakdown()
    {
        $client_id = Input::get('clientID');
        $visitors=VisitorBreakdown::where('client_id',$client_id)->first();
        $data['TotalUniqueIDs'] = $visitors->TotalUniqueusers;
        $data['UniqueIDsBlockingCookies'] = $visitors->UniqueConverions;
        $data['AveragePagesConsumed'] = $visitors->avgses;
        $data['TotalContentActivities'] = $visitors->TotalContentActivities;
        
        return response()->json($data);
    }
    public function getAccountBasedMarketing()
    {
        $client_id = Input::get('clientID');
        $abm=ABMaggregate::where('client_id',$client_id)->first();
        return response()->json($abm);

    }
    

    public function getChannelOverview()
    {
        $client_id = Input::get('clientID');

        $UniqueIds=UniqueIdReporting::where('client_id',$client_id)->first();
        $ContentActivitie=ContentActivitiesReporting::where('client_id',$client_id)->first();
        $Conversion=ConversionReporting::where('client_id',$client_id)->first();

        $data['UniqueIds'] = $UniqueIds;
        $data['ContentActivitie'] = $ContentActivitie;
        $data['Conversion'] = $Conversion;

        return response()->json($data);
    }
    public function getConversionDetails(){
        $client_id = Input::get('clientID');
         
        // $days = Input::get('days');
        // $fromDate = date("Y-m-d");
        // $toDate = date('Y-m-d', strtotime($fromDate. ' - '.$days.' days'));

        $conversion_forms=ConversionForms::where('client',$client_id)->orderBy('conversions','desc')->get();
        return response()->json($conversion_forms);
    }
    
}
