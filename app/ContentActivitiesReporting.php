<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentActivitiesReporting extends Model
{
    protected $table = 'contentactivitiesreporting_u';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
