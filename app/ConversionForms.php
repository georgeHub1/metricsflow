<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConversionForms extends Model
{
    protected $table = 'conversionforms_u';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
