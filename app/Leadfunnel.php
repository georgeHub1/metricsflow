<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leadfunnel extends Model
{
    protected $table = 'Leadfunnel';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
