<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leads_Information extends Model
{
    protected $table = 'Lead_Information';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
