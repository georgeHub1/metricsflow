<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ABMCVDetails extends Model
{
    protected $table = 'ABM_CV_Details_u';

    public $timestamps = false;

    protected $connection = 'raw-data';
}
