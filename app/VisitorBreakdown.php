<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitorBreakdown extends Model
{
    protected $table = 'VisitorBreakdown_u';

    public $timestamps = false;

    protected $connection = 'raw-data';


}
