@extends('template.template')

@section('content-header')
    <link rel="stylesheet" href="/plugins/material/material-charts.css">
    <link rel="stylesheet" href="{{URL::to('/css/report.css')}}">
    <style>
        .datagraph{
            float: left;
            width: 100%;
            padding-top: 50px;
        }
        .datagraph i{
            float: left;
            width: 100%;
            font-size: 28px;
            padding: 10px 0px;
        }
        .socialdata{
            float: left;
            width: 100%;
            border-radius: 5px;
            text-align: left;
            padding: 0px;
            box-shadow: 0px 2px 8px #bababa;
        }
        .socialdata .pagedata{
            background-color: #1E7DBF;
            color: #FFF;
            padding: 5px 10px;
            border-radius: 5px 5px 0px 0px;
            height: 50px;
        }
        .socialdata .warrantypagedata{
            background-color: #00B463;
            color: #FFF;
            padding: 5px 10px;
            border-radius: 5px 5px 0px 0px;
            height: 50px;
        }
        .socialdata .usercount{
            float: left;
            width: 100%;
            padding: 5px 10px;
        }
        .socialdata .usercount p{
            margin-bottom: 0px;
        }
        .datagraph .datadivpart1{
            padding-right: 0px;
        }
        .datagraph .datadivpart2{
            padding-left: 0px;
        }

        @media (min-width: 768px) and (max-width: 990px) {
            .datagraph .datadivpart2 {
                padding-top: 30px;
            }
        }
        @media (min-width: 990px) and (max-width: 1024px) {
            .socialdata .pagedata,
            .socialdata .usercount
            {
                font-size: 12px;
            }
        }
        @media (max-width: 480px) {
            .datagraph .datadivpart1 .datacommondiv,
            .datagraph .datadivpart2 .datacommondiv{
                padding-top: 30px;
            }
            .datagraph .datadivpart1{
                padding-right: 15px;
            }
            .datagraph .datadivpart2
            {
                padding-left: 15px;
            }
        }

        @media (min-width: 551px) and (max-width: 767px) {
            .datagraph .datadivpart1,
            .datagraph .datadivpart2
            {
                width: 50%;
                float: left;
            }
            .datagraph .datadivpart1 .datacommondiv,
            .datagraph .datadivpart2 .datacommondiv{
                padding-top: 30px;
            }
        }
    </style>
    <section class="content-header">

        <div id="breadcrumbs">
            <h4 class="bold"><i class="fa fa-chevron-left" aria-hidden="true"></i><a href="/Reports">Reports</a> <i class="fa fa-chevron-left" aria-hidden="true"></i> <a href="/ReportsDetail">Conversion Details</a> <i class="fa fa-chevron-left" aria-hidden="true"></i> Warranty Page</h4>
        </div>

    </section>
@endsection

@section('content')

    <section>
        <div class="row">
            <div class="col-lg-11 col-md-12 col-xs-12 col-sm-12">
                <h3 class="bold">Conversion Details For {{$PageName}} </h3>
            </div>
            <div class="col-lg-11 col-md-12 col-xs-12 col-sm-12" >
                <div class="alert-modal">
                    <div class="modal">
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12"  style="padding: 0px !important;">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-body row">
                                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                            <div class="row">

                                                <div class="datagraph">
                                                    <div class="col-md-6 col-sm-12 datadivpart1">

                                                    </div>
                                                    <div class="col-md-6 col-sm-12 datadivpart2">

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                        </div>
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </div>
        </div>
    </section>
    <script src="/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script>

        function removeNulls(obj){
            var isArray = obj instanceof Array;
            for (var k in obj){
                if (obj[k]===null) isArray ? obj.splice(k,1) : delete obj[k];
                else if (typeof obj[k]=="object") removeNulls(obj[k]);
            }
        }

        var sites = {!! json_encode($data->toArray()) !!} ;

        removeNulls(sites);

        for (var i=0; i< sites.length; i++)
        {
            var s = '.datacommondiv'+i;
            if( i <= 2){ $('.datadivpart1').append('<div class="col-md-4 col-sm-4 col-xs-12 text-center datacommondiv'+i+'"> </div>');}
            else{$('.datadivpart2').append('<div class="col-md-4 col-sm-4 col-xs-12 text-center datacommondiv'+i+'"> </div>');}

            function attach(div) {
                if( i <= 2){ $(s).append(div);}
                else{$(s).append(div);}

            }



            var channel = sites[i]['Channel'];
            var conversion = sites[i]['conversion_page_e_id_SESSION_WISE'];

            var div ='<div class="socialdata">\n' +
                // '        <div class="pagedata">Channel</div>\n' +
                '        <div class="pagedata">\n' +
                '            <h4 align="center">'+ channel +'</h4>\n' +
                '        </div>\n' +
                '    </div>' +
                '<i class="fa fa-chevron-down" aria-hidden="true"></i>';
            attach(div);
            delete sites[i]['Channel'];
            delete sites[i]['conversion_page_e_id_SESSION_WISE'];

            for (var key in sites[i]) {
                if (sites[i].hasOwnProperty(key)) {
                    var div = '<div class="socialdata">\n' +
                        '<div class="pagedata">'+sites[i][key]+'</div>\n' +
                        '<div class="usercount">\n' +
                        '<p><span>321</span> Users Entered</p>\n' +
                        '<p><span>78</span> Bounced</p>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '<i class="fa fa-chevron-down" aria-hidden="true"></i>';

                attach(div);
                }

            }

            var div = '<div class="socialdata">\n' +
                '                    <div class="warrantypagedata">'+conversion+'</div>\n' +
                '                <div class="usercount">\n' +
                '                    <p><span>33</span> Users Entered</p>\n' +
                '                <p><span>12</span> Converted</p>\n' +
                '                </div>\n' +
                '                </div>';
            attach(div);


        }




    </script>


@endsection

